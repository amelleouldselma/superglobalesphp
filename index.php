<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Superglobales, sessions et cookies en php</title>
</head>
<body>
<h1>VI superglobales, sessions et cookies</h1>
<!-- 
Dans tous les exercices, faites une page HTML 5 valide et soignez vos CSS. -->

<h1>Exercice 1 </h1>
<!-- Faire une page HTML permettant de donner à l'utilisateur :

    son User Agent
    son adresse ip
    le nom du serveur -->

 <?php 
    echo $_SERVER['HTTP_USER_AGENT']
  ?>

</br>

<?php 
    print "Adresse IP : ".$_SERVER['REMOTE_ADDR']
 ?>

<h1>Exercice 2 </h1>
<!-- Sur la page index, faire un liens vers une autre page. 
Passer d'une page à l'autre le contenu des variables nom, prenom et age grâce aux sessions. 
Ces variables auront été définies directement dans le code.
 Il faudra afficher le contenu de ces variables sur la deuxième page. -->

 <h1>Exercice 3 </h1>
<!-- Faire un formulaire qui permet de récupérer le login et le mot de passe de l'utilisateur. 
A la validation du formulaire, stocker les informations dans un cookie. -->

<h1>Exercice 4</h1>
 <!-- Faire une page qui va récupérer les informations du cookie créé à l'exercice 3 et qui les affiches. -->

 <h1>Exercice 5</h1>
 <!-- Faire une page qui va pouvoir modifier le contenu du cookie de l'exercice 3. -->
</body>
</html>